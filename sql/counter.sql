/*
 Navicat Premium Data Transfer

 Source Server         : 国英MYSQL
 Source Server Type    : MySQL
 Source Server Version : 50740
 Source Host           : 47.104.86.29:3306
 Source Schema         : counter

 Target Server Type    : MySQL
 Target Server Version : 50740
 File Encoding         : 65001

 Date: 16/10/2023 14:28:43
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cost
-- ----------------------------
DROP TABLE IF EXISTS `cost`;
CREATE TABLE `cost`  (
  `cost_id` int(11) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT COMMENT '费用id',
  `create_time` bigint(20) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(20) NULL DEFAULT NULL COMMENT '更新时间',
  `cost_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '费用名称',
  `cost_value` int(11) NULL DEFAULT NULL COMMENT '费用值',
  `coef` double NULL DEFAULT NULL COMMENT '系数',
  `user_id` int(11) NULL DEFAULT NULL,
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`cost_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cost
-- ----------------------------
INSERT INTO `cost` VALUES (00000000006, 1668404248639, 1671282332967, '固定费用', 200, 100, 1, '张三', NULL, NULL);

-- ----------------------------
-- Table structure for device
-- ----------------------------
DROP TABLE IF EXISTS `device`;
CREATE TABLE `device`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '设备id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设备名称',
  `device_cost` double NULL DEFAULT NULL COMMENT '设备单价',
  `desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设备描述',
  `salary` double NULL DEFAULT NULL COMMENT '其中工资',
  `install_cost` double NULL DEFAULT NULL COMMENT '安装费',
  `unit` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设备单位',
  `create_time` bigint(20) NULL DEFAULT NULL,
  `update_time` bigint(20) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type_id` int(11) NULL DEFAULT NULL,
  `device_cost_gong` double NULL DEFAULT NULL,
  `type1_id` int(11) NULL DEFAULT NULL,
  `type2_id` int(11) NULL DEFAULT NULL,
  `type3_id` int(11) NULL DEFAULT NULL,
  `debug_cost` double NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 90 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of device
-- ----------------------------
INSERT INTO `device` VALUES (26, '电力电缆其他敷设，35mm²以下，铝缆', 0, NULL, 360.48, 495.71, '100/m', NULL, 1686386854846, 14, '张士峰', '主材', 2, 0, 9, 16, 35, 1328.14);
INSERT INTO `device` VALUES (27, '电力电缆其他敷设，10mm²以下，铝缆', 0, NULL, 239.16, 359.33, '100m', NULL, 1686387152780, 14, '张士峰', '主材', 2, 0, 9, 16, 36, 1328.14);
INSERT INTO `device` VALUES (28, '电力电缆其他敷设，400mm²以下，铜缆', 0, NULL, 1975.8, 2998.28, '100m', NULL, 1686387162129, 14, '张士峰', '主材', 2, 0, 9, 17, 32, 1328.14);
INSERT INTO `device` VALUES (29, '电力电缆其他敷设，240mm²以下，铜缆', 0, NULL, 1282.68, 1762.32, '100m', NULL, 1686387169282, 14, '张士峰', '主材', 2, 0, 9, 17, 33, 1328.14);
INSERT INTO `device` VALUES (30, '电力电缆其他敷设，120mm²以下，铜缆', 0, NULL, 909.96, 1096.73, '100m', NULL, 1686387175662, 14, '张士峰', '主材', 2, 0, 9, 17, 34, 1328.14);
INSERT INTO `device` VALUES (31, '电力电缆其他敷设，35mm²以下，铜缆', 0, NULL, 504.96, 640.19, '100m', NULL, 1686387182100, 14, '张士峰', '主材', 2, 0, 9, 17, 35, 1328.14);
INSERT INTO `device` VALUES (32, '电力电缆其他敷设，10mm²以下，铜缆', 0, NULL, 334.68, 454.85, '100m', NULL, 1686387188459, 14, '张士峰', '主材', 2, 0, 9, 17, 36, 1328.14);
INSERT INTO `device` VALUES (33, '户内冷缩式电力电缆头 铝芯终端头10kV以下截面35m²以下', 0, NULL, 52.2, 129.66, '个', NULL, 1672654272950, 1, '张三', '主材', 2, 0, 10, 18, 40, 0);
INSERT INTO `device` VALUES (34, '户内冷缩式电力电缆头 铝芯终端头10kV以下截面 120m²以下', 0, NULL, 80.76, 198.03, '个', NULL, 1672654290657, 1, '张三', '主材', 2, 0, 10, 18, 39, 0);
INSERT INTO `device` VALUES (35, '户内冷缩式电力电缆头 铝芯终端头10kV以下截面 240m²以下', 0, NULL, 104.52, 266.65, '个', NULL, 1672654303550, 1, '张三', '主材', 2, 0, 10, 18, 38, 0);
INSERT INTO `device` VALUES (36, '户内冷缩式电力电缆头 铝芯终端头10kV以下截面 400m²以下', 0, NULL, 149.64, 1088.18, '个', NULL, 1672654314832, 1, '张三', '主材', 2, 0, 10, 18, 37, 0);
INSERT INTO `device` VALUES (37, '户内冷缩式电力电缆头 铜芯终端头10kV以下截面35m²以下', 0, NULL, 62.88, 145.59, '个', NULL, 1672654325612, 1, '张三', '主材', 2, 0, 10, 19, 40, 0);
INSERT INTO `device` VALUES (38, '户内冷缩式电力电缆头 铜芯终端头10kV以下截面 120m²以下', 0, NULL, 97.2, 233.71, '个', NULL, 1672654335700, 1, '张三', '主材', 2, 0, 10, 19, 39, 0);
INSERT INTO `device` VALUES (39, '户内冷缩式电力电缆头 铜芯终端头10kV以下截面 240m²以下', 0, NULL, 125.52, 333.61, '个', NULL, 1672654345066, 1, '张三', '主材', 2, 0, 10, 19, 38, 0);
INSERT INTO `device` VALUES (40, '户内冷缩式电力电缆头 铜芯终端头10kV以下截面 400m²以下', 0, NULL, 176.64, 531.87, '个', NULL, 1672654377992, 1, '张三', '主材', 2, 0, 10, 19, 37, 0);
INSERT INTO `device` VALUES (55, '油浸电力变压器2500kVA', 345200, '无', 3775.44, 5803.82, '台', 1671072681512, 1672654421301, 1, '张三', '设备', 1, 345200, 5, 11, 25, 7337.11);
INSERT INTO `device` VALUES (56, '油浸电力变压器2000kVA', 211700, '无', 2095.56, 3485.33, '台', 1671072816957, 1672654430348, 1, '张三', '设备', 1, 211700, 5, 11, 26, 6502.46);
INSERT INTO `device` VALUES (57, '油浸电力变压器1600kVA', 182500, '无', 2095.56, 3485.33, '台', 1671072866014, 1672654439085, 1, '张三', '设备', 1, 182500, 5, 11, 27, 6502.46);
INSERT INTO `device` VALUES (58, '油浸电力变压器1250kVA', 147400, '无', 2095.56, 3485.33, '台', 1671073099657, 1672654448038, 1, '张三', '设备', 1, 147400, 5, 11, 28, 6502.46);
INSERT INTO `device` VALUES (59, '油浸电力变压器1000kVA', 123800, '无', 1617.6, 2734.05, '台', 1671073136453, 1672654457164, 1, '张三', '设备', 1, 123800, 5, 11, 29, 6502.46);
INSERT INTO `device` VALUES (60, '油浸电力变压器800kVA', 117800, '无', 1617.6, 2734.05, '台', 1671073191586, 1672654466980, 1, '张三', '设备', 1, 117800, 5, 11, 30, 3043.45);
INSERT INTO `device` VALUES (61, '油浸电力变压器400kVA', 67900, '无', 1617.6, 2734.05, '台', 1671073209976, 1672654477336, 1, '张三', '设备', 1, 67900, 5, 11, 31, 2275.34);
INSERT INTO `device` VALUES (62, '干式电力变压器2500kVA', 303000, '无', 1671.84, 2338.01, '台', 1671078083661, 1672704844791, 1, '张三', '设备', 1, 303000, 5, 12, 25, 7337.11);
INSERT INTO `device` VALUES (63, '干式电力变压器2000kVA', 269300, '无', 1393.32, 1998.04, '台', 1671078231206, 1672704854323, 1, '张三', '设备', 1, 269300, 5, 12, 26, 6502.46);
INSERT INTO `device` VALUES (64, '干式电力变压器1600kVA', 233300, '无', 1393.32, 1998.04, '台', 1671078259392, 1672704863556, 1, '张三', '设备', 1, 233300, 5, 12, 27, 6502.46);
INSERT INTO `device` VALUES (65, '干式电力变压器1250kVA', 181800, '无', 1393.32, 1998.04, '台', 1671078288863, 1672704873002, 1, '张三', '设备', 1, 181800, 5, 12, 28, 6502.46);
INSERT INTO `device` VALUES (66, '干式电力变压器1000kVA', 155700, '无', 1167.48, 1706.74, '台', 1671082619866, 1672704883699, 1, '张三', '设备', 1, 155700, 5, 12, 29, 6502.46);
INSERT INTO `device` VALUES (67, '干式电力变压器800kVA', 132000, '无', 1057.44, 1349.52, '台', 1671082757828, 1672704917421, 1, '张三', '设备', 1, 132000, 5, 12, 30, 3043.45);
INSERT INTO `device` VALUES (68, '干式电力变压器400kVA', 88700, '无', 1057.44, 1349.52, '台', 1671082772213, 1672704932076, 1, '张三', '设备', 1, 88700, 5, 12, 31, 2275.34);
INSERT INTO `device` VALUES (69, '10kV高压柜KYN28，断路器柜', 20000, '无', 775.2, 922.34, '面', 1671082801410, 1672704945100, 1, '张三', '设备', 1, 20000, 6, 13, 0, 0);
INSERT INTO `device` VALUES (70, '10kV高压柜KYN28，互感器柜', 20000, '无', 618.36, 734.68, '面', 1671082819393, 1672704954512, 1, '张三', '设备', 1, 20000, 6, 14, 0, 0);
INSERT INTO `device` VALUES (71, '10kV高压柜KYN28，隔离手车柜', 40000, '无', 371.28, 486.66, '面', 1671082837545, 1672704962899, 1, '张三', '设备', 1, 40000, 6, 15, 0, 0);
INSERT INTO `device` VALUES (72, '低压成套配电柜(屏)', 40000, '无', 528.6, 668.38, '台', 1671082858911, 1672704987950, 1, '张三', '设备', 1, 40000, 8, 0, 0, 0);
INSERT INTO `device` VALUES (73, '低压电容器柜MNS', 40000, '无', 215.64, 363.48, '面', 1671082918396, 1672704999244, 1, '张三', '设备', 1, 40000, 7, 0, 0, 0);
INSERT INTO `device` VALUES (74, '直流馈电屏-1', 19000, '无', 270.96, 376.28, '台', 1671082943574, 1672705006667, 1, '张三', '设备', 1, 19000, 8, 0, 0, 0);
INSERT INTO `device` VALUES (87, '电力电缆其他敷设，400m²以下，铝缆', 0, '', 1411.32, 2206.25, '100m', 1671196286141, 1672705622702, 1, '张三', '主材', 2, 0, 9, 16, 32, 1328.14);
INSERT INTO `device` VALUES (88, '电力电缆其他敷设，240m²以下，铝缆', 0, '', 916.44, 1307.7, '100m', 1671196328365, 1672705630781, 1, '张三', '主材', 2, 0, 9, 16, 33, 1328.14);
INSERT INTO `device` VALUES (89, '电力电缆其他敷设，120m²以下，铝缆', 0, '', 650.04, 817.8, '100m', 1671196382154, 1672705642334, 1, '张三', '主材', 2, 0, 9, 16, 34, 1328.14);

-- ----------------------------
-- Table structure for device_order
-- ----------------------------
DROP TABLE IF EXISTS `device_order`;
CREATE TABLE `device_order`  (
  `order_id` int(11) NOT NULL COMMENT '订单id',
  `device_id` int(11) NULL DEFAULT NULL COMMENT '设备id',
  `device_count` int(11) NULL DEFAULT NULL COMMENT '设备数量',
  PRIMARY KEY (`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of device_order
-- ----------------------------

-- ----------------------------
-- Table structure for enum
-- ----------------------------
DROP TABLE IF EXISTS `enum`;
CREATE TABLE `enum`  (
  `enum_type` int(11) NULL DEFAULT NULL COMMENT '属于什么大类型',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` int(11) NOT NULL COMMENT '类型值',
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型名称',
  `create_time` bigint(20) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(20) NULL DEFAULT NULL COMMENT '更新时间',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '最后更新人',
  `level` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 41 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of enum
-- ----------------------------
INSERT INTO `enum` VALUES (2, 5, 5, '变压器', NULL, NULL, NULL, 1);
INSERT INTO `enum` VALUES (3, 6, 6, '高压柜', NULL, NULL, NULL, 1);
INSERT INTO `enum` VALUES (4, 7, 7, '低压柜', NULL, NULL, NULL, 1);
INSERT INTO `enum` VALUES (5, 8, 8, '屏', NULL, NULL, NULL, 1);
INSERT INTO `enum` VALUES (6, 9, 9, '电缆', NULL, NULL, NULL, 1);
INSERT INTO `enum` VALUES (7, 10, 10, '电缆头', NULL, NULL, NULL, 1);
INSERT INTO `enum` VALUES (2, 11, 11, '油浸', NULL, NULL, NULL, 2);
INSERT INTO `enum` VALUES (2, 12, 12, '干式', NULL, NULL, NULL, 2);
INSERT INTO `enum` VALUES (3, 13, 13, '断路器柜', NULL, NULL, NULL, 2);
INSERT INTO `enum` VALUES (3, 14, 14, '互感器柜', NULL, NULL, NULL, 2);
INSERT INTO `enum` VALUES (3, 15, 15, '隔离手车柜', NULL, NULL, NULL, 2);
INSERT INTO `enum` VALUES (6, 16, 16, '铝缆', NULL, NULL, NULL, 2);
INSERT INTO `enum` VALUES (6, 17, 17, '铜缆', NULL, NULL, NULL, 2);
INSERT INTO `enum` VALUES (7, 18, 18, '铝芯', NULL, NULL, NULL, 2);
INSERT INTO `enum` VALUES (7, 19, 19, ' 铜芯', NULL, NULL, NULL, 2);
INSERT INTO `enum` VALUES (2, 25, 25, '2500kVA', NULL, NULL, NULL, 3);
INSERT INTO `enum` VALUES (2, 26, 26, '2000kVA', NULL, NULL, NULL, 3);
INSERT INTO `enum` VALUES (2, 27, 27, '1600kVA', NULL, NULL, NULL, 3);
INSERT INTO `enum` VALUES (2, 28, 28, '1250kVA', NULL, NULL, NULL, 3);
INSERT INTO `enum` VALUES (2, 29, 29, '1000kVA', NULL, NULL, NULL, 3);
INSERT INTO `enum` VALUES (2, 30, 30, '800kVA', NULL, NULL, NULL, 3);
INSERT INTO `enum` VALUES (2, 31, 31, '400kVA', NULL, NULL, NULL, 3);
INSERT INTO `enum` VALUES (6, 32, 32, '400㎡以下', NULL, NULL, NULL, 3);
INSERT INTO `enum` VALUES (6, 33, 33, '240㎡以下', NULL, NULL, NULL, 3);
INSERT INTO `enum` VALUES (6, 34, 34, '120㎡以下', NULL, NULL, NULL, 3);
INSERT INTO `enum` VALUES (6, 35, 35, '35㎡以下', NULL, NULL, NULL, 3);
INSERT INTO `enum` VALUES (6, 36, 36, '10㎡以下', NULL, NULL, NULL, 3);
INSERT INTO `enum` VALUES (7, 37, 37, '10kV以下 | 400㎡以下', NULL, NULL, NULL, 3);
INSERT INTO `enum` VALUES (7, 38, 38, '10kV以下 | 240㎡以下', NULL, NULL, NULL, 3);
INSERT INTO `enum` VALUES (7, 39, 39, '10kV以下 | 120㎡以下', NULL, NULL, NULL, 3);
INSERT INTO `enum` VALUES (7, 40, 40, '10kV以下 | 35㎡以下', NULL, NULL, NULL, 3);

-- ----------------------------
-- Table structure for project
-- ----------------------------
DROP TABLE IF EXISTS `project`;
CREATE TABLE `project`  (
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '项目名称',
  `id` int(11) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT COMMENT '项目id',
  `create_time` bigint(20) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(20) NULL DEFAULT NULL COMMENT '更新时间',
  `devices` json NULL COMMENT '设备json',
  `costs` json NULL COMMENT '费用json',
  `amount` double NULL DEFAULT NULL COMMENT '总价',
  `status` int(11) NULL DEFAULT NULL COMMENT '状态',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '创建用户的id',
  `project_type` int(11) NULL DEFAULT NULL,
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `price_type` int(11) NULL DEFAULT NULL,
  `designCostJson` json NULL COMMENT '设计费内容',
  `structure_area` double NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `创建时间`(`create_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of project
-- ----------------------------
INSERT INTO `project` VALUES ('配电工程估算书（工业）', 00000000013, 1668404851589, 1668404851589, '\"[{\\\"device_id\\\":1,\\\"device_name\\\":\\\"油浸式变压器10kV/容量4000kV·A以下\\\",\\\"count\\\":1,\\\"type\\\":1},{\\\"device_id\\\":7,\\\"device_name\\\":\\\"油浸式变压器10kV/容量2000kV·A以下\\\",\\\"count\\\":1,\\\"type\\\":1},{\\\"device_id\\\":9,\\\"device_name\\\":\\\"干式变压器10kV/容量4000kV·A以下\\\",\\\"count\\\":1,\\\"type\\\":1}]\"', '\"[]\"', 6039, 1, 1, 2, '张三', 2, NULL, NULL);
INSERT INTO `project` VALUES ('10kV铜雀线线路迁改', 00000000015, 1670161204426, 1684914203411, '\"[]\"', '\"[{\\\"cost_name\\\":\\\"固定费用\\\",\\\"count\\\":200}]\"', 52450, 1, 1, 1, '张三', 1, '\"{\\\"value1\\\":\\\"50000\\\",\\\"value2\\\":\\\"222312\\\",\\\"value3\\\":\\\"859332\\\",\\\"value4\\\":0,\\\"value5\\\":0,\\\"value6\\\":0,\\\"total2\\\":\\\"2250.00\\\",\\\"total3\\\":\\\"0.49\\\",\\\"total4\\\":\\\"0.39\\\",\\\"amount2\\\":0,\\\"amount3\\\":0}\"', 100);
INSERT INTO `project` VALUES ('10KV金雀线工程', 00000000016, 1671199062791, 1684914002852, '\"[{\\\"device_id\\\":26,\\\"device_name\\\":\\\"电力电缆其他敷设，35m²以下，铝缆\\\",\\\"count\\\":1,\\\"type\\\":1}]\"', '\"[{\\\"cost_name\\\":\\\"固定费用\\\",\\\"count\\\":200}]\"', 7707.62, 1, 1, 1, '张三', 1, '\"{\\\"value1\\\":\\\"5000\\\",\\\"value2\\\":\\\"200\\\",\\\"value3\\\":\\\"859332\\\",\\\"value4\\\":0,\\\"value5\\\":0,\\\"value6\\\":0,\\\"total2\\\":\\\"323.29\\\",\\\"total3\\\":\\\"0.39\\\",\\\"total4\\\":\\\"0.31\\\",\\\"amount2\\\":1688.62,\\\"amount3\\\":495.71}\"', 10);
INSERT INTO `project` VALUES ('临港输电线路迁改工程', 00000000028, 1671592258344, 1685665440032, '\"[{\\\"device_id\\\":26,\\\"device_name\\\":\\\"电力电缆其他敷设，35m²以下，铝缆\\\",\\\"count\\\":1,\\\"type\\\":1},{\\\"device_id\\\":30,\\\"device_name\\\":\\\"电力电缆其他敷设，120m²以下，铜缆\\\",\\\"count\\\":1,\\\"type\\\":1}]\"', '\"[{\\\"cost_name\\\":\\\"固定费用\\\",\\\"count\\\":200}]\"', 58217.52, 1, 14, 1, '张士峰', 1, '\"{\\\"value1\\\":\\\"50000\\\",\\\"value2\\\":0,\\\"value3\\\":0,\\\"value4\\\":0,\\\"value5\\\":0,\\\"value6\\\":0,\\\"total2\\\":\\\"2498.36\\\",\\\"total3\\\":\\\"0.00\\\",\\\"total4\\\":\\\"0.00\\\",\\\"amount2\\\":3926.72,\\\"amount3\\\":1592.44}\"', 100);
INSERT INTO `project` VALUES ('10KV银雀线工程', 00000000029, 1672729712640, 1684914085309, '\"[{\\\"device_id\\\":63,\\\"device_name\\\":\\\"干式电力变压器2000kVA\\\",\\\"count\\\":1,\\\"type\\\":1},{\\\"device_id\\\":87,\\\"device_name\\\":\\\"电力电缆其他敷设，400m²以下，铝缆\\\",\\\"count\\\":1,\\\"type\\\":1}]\"', '\"[{\\\"cost_name\\\":\\\"固定费用\\\",\\\"count\\\":200}]\"', 302350.81, 1, 1, 1, '张三', 2, '\"{\\\"value1\\\":\\\"5000\\\",\\\"value2\\\":0,\\\"value3\\\":0,\\\"value4\\\":0,\\\"value5\\\":0,\\\"value6\\\":0,\\\"total2\\\":\\\"13011.28\\\",\\\"total3\\\":\\\"0.00\\\",\\\"total4\\\":\\\"0.00\\\",\\\"amount2\\\":279935.24,\\\"amount3\\\":4204.29}\"', 10);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `user_phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户手机号',
  `create_time` bigint(20) NULL DEFAULT NULL COMMENT '创建时间',
  `user_role` int(11) NULL DEFAULT NULL COMMENT '用户角色',
  `nick_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户昵称',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('杨佳宇', 1, '13290244775', 1667052757760, 1, '冬夏');
INSERT INTO `user` VALUES ('绪博', 11, '1111', 1668410369268, 2, '1111');
INSERT INTO `user` VALUES ('孔令辉', 12, '3333', 1668410906007, 3, '3333');
INSERT INTO `user` VALUES ('李超', 13, '13791500307', 1671353103151, 1, '13791500307');
INSERT INTO `user` VALUES ('张士峰', 14, '15006495278', 1671353460437, 1, '15006495278');
INSERT INTO `user` VALUES ('杨晨瑶', 15, '222', 1671373418191, 2, '222');
INSERT INTO `user` VALUES ('包木森', 16, '测试1', 1671387585379, 1, '测试1');
INSERT INTO `user` VALUES ('王砥凡', 17, '18805399366', 1671410801354, 1, '18805399366');
INSERT INTO `user` VALUES ('公杰', 18, '1111', 1675301075230, 2, '1111');

SET FOREIGN_KEY_CHECKS = 1;
