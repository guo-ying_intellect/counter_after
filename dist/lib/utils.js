"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.formatDatetime = formatDatetime;
exports.checkUserIsAdmin = checkUserIsAdmin;
exports.findUserByPhone = findUserByPhone;

var _auth = require("../controllers/auth");

var _User = _interopRequireDefault(require("../models/User"));

function formatDatetime(date) {
  if (!date) {
    return "";
  }

  if (typeof date == "number" || typeof date == 'string') {
    date = new Date(date);
  }

  function fullNumToStr(num) {
    return num < 10 ? `0${num}` : `${num}`;
  }

  if (!date) return '-';
  let year = date.getFullYear();
  let month = fullNumToStr(date.getMonth() + 1);
  let day = fullNumToStr(date.getDate());
  let hour = fullNumToStr(date.getHours());
  let minute = fullNumToStr(date.getMinutes());
  let second = fullNumToStr(date.getSeconds());
  return `${year}-${month}-${day} ${hour}:${minute}:${second}`;
} // 检测用户是不是管理员  通过传入的id


async function checkUserIsAdmin(ctx) {
  let {
    data
  } = (0, _auth.CheckAuth)(ctx);

  if (data) {
    let user = await findUserByPhone(null, data);
    return user && user.user_role === 1;
  } else {
    return false;
  }
}

async function findUserByPhone(ctx, phone) {
  let userPhone = phone || ctx.request.body.userPhone;
  if (!userPhone) return null;
  let user = await _User.default.findOne({
    where: {
      user_phone: userPhone
    }
  });
  return user;
}