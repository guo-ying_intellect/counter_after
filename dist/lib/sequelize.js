"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _sequelize = require("sequelize");

var _config = require("../config");

const sequelize = new _sequelize.Sequelize(_config.DB.database, _config.DB.username, _config.DB.password, {
  host: _config.DB.host,
  port: _config.DB.port,
  dialect: _config.System.db_type,
  dialectOptions: {
    // MySQL > 5.5，其它数据库删除此项
    charset: 'utf8mb4',
    supportBigNumbers: true,
    bigNumberStrings: true
  },
  pool: {
    max: 50,
    min: 0,
    idle: 10000
  }
}); // 测试是否能连通

sequelize.authenticate().then(() => {
  console.log("连接数据库成功");
}).catch(err => {
  console.log("连接数据库失败", err);
});
var _default = sequelize;
exports.default = _default;