"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getCityUnionJsonFile = exports.getCityUnionFilePath = void 0;

var _fs = _interopRequireDefault(require("fs"));

var _path = _interopRequireDefault(require("path"));

const getCityUnionFilePath = fileName => {
  return _path.default.resolve(__dirname, `../../city-union-json/${fileName}`);
};

exports.getCityUnionFilePath = getCityUnionFilePath;

const getCityUnionJsonFile = async filePath => {
  return new Promise((resolve, reject) => {
    _fs.default.readFile(filePath, {
      encoding: "utf-8"
    }, (err, fileStr) => {
      if (err) {
        resolve("{}");
      } else {
        resolve(fileStr);
      }
    });
  });
};

exports.getCityUnionJsonFile = getCityUnionJsonFile;