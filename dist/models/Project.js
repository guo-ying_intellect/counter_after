"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _sequelize = _interopRequireDefault(require("../lib/sequelize"));

var _sequelize2 = require("sequelize");

const projectModal = _sequelize.default.define('project', {
  name: {
    type: _sequelize2.DataTypes.STRING
  },
  id: {
    type: _sequelize2.DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  devices: {
    type: _sequelize2.DataTypes.JSON
  },
  create_time: {
    type: _sequelize2.DataTypes.BIGINT
  },
  update_time: {
    type: _sequelize2.DataTypes.BIGINT
  },
  costs: {
    type: _sequelize2.DataTypes.JSON
  },
  amount: {
    type: _sequelize2.DataTypes.DOUBLE
  },
  status: {
    type: _sequelize2.DataTypes.INTEGER
  },
  user_id: {
    type: _sequelize2.DataTypes.INTEGER
  },
  project_type: _sequelize2.DataTypes.INTEGER,
  price_type: _sequelize2.DataTypes.INTEGER,
  user_name: _sequelize2.DataTypes.STRING
}, {
  freezeTableName: true,
  timestamps: false
});

var _default = projectModal;
exports.default = _default;