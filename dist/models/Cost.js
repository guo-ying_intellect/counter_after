"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _sequelize = _interopRequireDefault(require("../lib/sequelize"));

var _sequelize2 = require("sequelize");

const costModal = _sequelize.default.define('cost', {
  cost_name: {
    type: _sequelize2.DataTypes.STRING
  },
  cost_id: {
    type: _sequelize2.DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  cost_value: {
    type: _sequelize2.DataTypes.DOUBLE
  },
  create_time: {
    type: _sequelize2.DataTypes.BIGINT
  },
  update_time: {
    type: _sequelize2.DataTypes.BIGINT
  },
  coef: {
    type: _sequelize2.DataTypes.DOUBLE
  }
}, {
  freezeTableName: true,
  timestamps: false
});

var _default = costModal;
exports.default = _default;