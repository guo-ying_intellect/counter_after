"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _sequelize = _interopRequireDefault(require("../lib/sequelize"));

var _sequelize2 = require("sequelize");

const userModal = _sequelize.default.define('user', {
  user_name: {
    type: _sequelize2.DataTypes.STRING
  },
  user_id: {
    type: _sequelize2.DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  user_phone: {
    type: _sequelize2.DataTypes.INTEGER
  },
  create_time: {
    type: _sequelize2.DataTypes.BIGINT
  },
  user_role: {
    type: _sequelize2.DataTypes.INTEGER
  },
  nick_name: {
    type: _sequelize2.DataTypes.STRING
  }
}, {
  freezeTableName: true,
  timestamps: false
});

var _default = userModal;
exports.default = _default;