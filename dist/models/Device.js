"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _sequelize = _interopRequireDefault(require("../lib/sequelize"));

var _sequelize2 = require("sequelize");

const deviceModal = _sequelize.default.define('device', {
  name: {
    type: _sequelize2.DataTypes.STRING
  },
  id: {
    type: _sequelize2.DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  device_cost: {
    type: _sequelize2.DataTypes.DOUBLE
  },
  create_time: {
    type: _sequelize2.DataTypes.BIGINT
  },
  update_time: {
    type: _sequelize2.DataTypes.BIGINT
  },
  desc: {
    type: _sequelize2.DataTypes.STRING
  },
  salary: {
    type: _sequelize2.DataTypes.DOUBLE
  },
  install_cost: {
    type: _sequelize2.DataTypes.DOUBLE
  },
  unit: {
    type: _sequelize2.DataTypes.STRING
  }
}, {
  freezeTableName: true,
  timestamps: false
});

var _default = deviceModal;
exports.default = _default;