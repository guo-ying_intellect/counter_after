"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Post = exports.CheckAuth = exports.Auth = void 0;

var _jsonwebtoken = _interopRequireWildcard(require("jsonwebtoken"));

var _fs = _interopRequireDefault(require("fs"));

var _path = _interopRequireDefault(require("path"));

const publicKey = _fs.default.readFileSync(_path.default.join(__dirname, '../../publicKey.pub'));

const Auth = {
  // 登录时生成token
  sign: (ctx, userInfo) => {
    let secret = publicKey + +new Date(); // 过期时间 一天 number = 秒  1d 10h

    const token = _jsonwebtoken.default.sign(userInfo, publicKey, {
      expiresIn: '1d'
    });

    ctx.set('Authorization', `Bearer ${token}`);
    return token;
  },
  verify: (ctx, decodedToken, token) => {
    let ret = true;

    try {
      const payload = _jsonwebtoken.default.verify(token, publicKey);

      ret = false;
    } catch (err) {
      console.log(`err.name`, err.name);
    }

    return ret;
  }
}; // 用户登录的时候返回token
// let token = jwt.sign({
//   userInfo: userInfo // 你要保存到token的数据
// }, publicKey, { expiresIn: '7d' })

/**
 * 检查授权是否合法
 */

exports.Auth = Auth;

const CheckAuth = ctx => {
  const token = ctx.request.header.authorization;
  console.log(`token`, token);

  try {
    const decoded = _jsonwebtoken.default.verify(token.substr(7), publicKey);

    console.log(decoded);

    if (decoded.userPhone) {
      return {
        code: 200,
        data: decoded.userPhone
      };
    } else {
      return {
        code: 400,
        msg: '非管理员不能进行此操作'
      };
    }
  } catch (err) {
    return {
      code: 400,
      msg: '非管理员不能进行此操作'
    };
  }
};

exports.CheckAuth = CheckAuth;

const Post = (ctx, next) => {
  switch (ctx.params.action) {
    case 'check':
      return CheckAuth(ctx).then(result => {
        ctx.body = result;
        next();
      });

    default:
      return CheckAuth(ctx).then(result => {
        ctx.body = result;
        next();
      });
  }
};

exports.Post = Post;