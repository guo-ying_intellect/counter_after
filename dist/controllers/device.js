"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.removeDevice = exports.updateDevice = exports.addDevice = exports.getDeviceById = exports.getDeviceList = void 0;

var _auth = require("./auth");

var _Device = _interopRequireDefault(require("../models/Device"));

var _sequelize = require("sequelize");

var _utils = require("../lib/utils");

// 获取设备列表 手机号/用户名模糊查询
const getDeviceList = async (ctx, next) => {
  let {
    searchText
  } = ctx.request.body;
  let list;

  if (searchText) {
    list = await _Device.default.findAll({
      where: {
        [_sequelize.Op.or]: [{
          name: {
            [_sequelize.Op.like]: `%${searchText}%` //模糊搜索

          }
        }]
      }
    });
  } else {
    list = await _Device.default.findAll();
  }

  ctx.body = {
    code: 200,
    data: list,
    msg: '请求成功'
  };
  next();
}; // 获取设备详情


exports.getDeviceList = getDeviceList;

const getDeviceById = async (ctx, next) => {
  let {
    id
  } = ctx.request.body;
  let isAdmin = await (0, _utils.checkUserIsAdmin)(ctx);

  if (isAdmin && id) {
    let device = await _Device.default.findOne({
      where: {
        id: id
      }
    });
    ctx.body = {
      data: device,
      code: 200,
      msg: '成功'
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作'
    };
  }

  next();
}; // 新增设备


exports.getDeviceById = getDeviceById;

const addDevice = async (ctx, next) => {
  let {
    name,
    device_cost,
    desc,
    salary,
    install_cost,
    unit
  } = ctx.request.body;
  let isAdmin = await (0, _utils.checkUserIsAdmin)(ctx);

  if (isAdmin) {
    let device = await _Device.default.create({
      name,
      device_cost,
      desc,
      salary,
      install_cost,
      unit,
      create_time: +new Date(),
      update_time: +new Date()
    });
    ctx.body = {
      data: null,
      code: 200,
      msg: '设备添加成功'
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作'
    };
  }

  next();
}; // 更新设备信息


exports.addDevice = addDevice;

const updateDevice = async (ctx, next) => {
  let {
    id,
    name,
    device_cost,
    desc,
    salary,
    install_cost,
    unit
  } = ctx.request.body;
  let isAdmin = await (0, _utils.checkUserIsAdmin)(ctx);

  if (isAdmin && id) {
    let device = await _Device.default.update({
      name,
      device_cost,
      desc,
      salary,
      install_cost,
      unit,
      update_time: +new Date()
    }, {
      where: {
        id
      }
    });
    ctx.body = {
      data: null,
      code: 200,
      msg: '设备更新成功'
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作'
    };
  }

  next();
}; // 删除设备


exports.updateDevice = updateDevice;

const removeDevice = async (ctx, next) => {
  let {
    id
  } = ctx.request.body;
  let isAdmin = await (0, _utils.checkUserIsAdmin)(ctx);

  if (isAdmin && id) {
    let device = await _Device.default.destroy({
      where: {
        id
      }
    });
    ctx.body = {
      data: null,
      code: 200,
      msg: '删除成功'
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作'
    };
  }

  next();
};

exports.removeDevice = removeDevice;