"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.removeProject = exports.updateProject = exports.addProject = exports.getProjectById = exports.getProjectList = void 0;

var _auth = require("./auth");

var _Project = _interopRequireDefault(require("../models/Project"));

var _sequelize = require("sequelize");

var _utils = require("../lib/utils");

// 获取工程列表 手机号/用户名模糊查询
const getProjectList = async (ctx, next) => {
  let {
    searchText,
    pageSize
  } = ctx.request.body;
  let list;
  let option = {};

  if (searchText) {
    option.where = {
      [_sequelize.Op.or]: [{
        name: {
          [_sequelize.Op.like]: `%${searchText}%` //模糊搜索

        }
      }]
    };
  }

  if (pageSize) {
    option.limit = pageSize;
  }

  list = await _Project.default.findAll(option);
  ctx.body = {
    code: 200,
    data: list,
    msg: '请求成功'
  };
  next();
}; // 获取设备详情


exports.getProjectList = getProjectList;

const getProjectById = async (ctx, next) => {
  let {
    id
  } = ctx.request.body;
  let isAdmin = await (0, _utils.checkUserIsAdmin)(ctx);

  if (isAdmin && id) {
    let project = await _Project.default.findOne({
      where: {
        id
      }
    });
    ctx.body = {
      data: project,
      code: 200,
      msg: '成功'
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作'
    };
  }

  next();
}; // 新增费用类型


exports.getProjectById = getProjectById;

const addProject = async (ctx, next) => {
  let {
    name,
    devices,
    costs,
    amount,
    status,
    project_type,
    price_type
  } = ctx.request.body;
  let isAdmin = await (0, _utils.checkUserIsAdmin)(ctx);

  if (isAdmin) {
    let {
      data
    } = (0, _auth.CheckAuth)(ctx);
    let user = await (0, _utils.findUserByPhone)(null, data);
    let cost = await _Project.default.create({
      name,
      devices,
      costs,
      amount,
      project_type,
      price_type,
      user_id: user.user_id,
      user_name: user.user_name,
      status: status || 1,
      // 状态暂时不需要
      create_time: +new Date(),
      update_time: +new Date()
    });
    ctx.body = {
      data: null,
      code: 200,
      msg: '项目添加成功'
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作'
    };
  }

  next();
}; // 更新项目信息


exports.addProject = addProject;

const updateProject = async (ctx, next) => {
  let {
    id,
    name,
    devices,
    costs,
    amount,
    status,
    project_type,
    price_type
  } = ctx.request.body;
  let isAdmin = await (0, _utils.checkUserIsAdmin)(ctx);

  if (isAdmin && id) {
    let {
      data
    } = (0, _auth.CheckAuth)(ctx);
    let user = await (0, _utils.findUserByPhone)(null, data); // TODO 增加非创建人能否修改

    let project = await _Project.default.update({
      name,
      devices,
      costs,
      amount,
      project_type,
      price_type,
      status: status || 1,
      // 状态暂时不需要
      update_time: +new Date()
    }, {
      where: {
        id
      }
    });
    ctx.body = {
      data: null,
      code: 200,
      msg: '费用更新成功'
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作'
    };
  }

  next();
}; // 删除设备


exports.updateProject = updateProject;

const removeProject = async (ctx, next) => {
  let {
    id
  } = ctx.request.body;
  let isAdmin = await (0, _utils.checkUserIsAdmin)(ctx);

  if (isAdmin && id) {
    let project = await _Project.default.destroy({
      where: {
        id
      }
    });
    ctx.body = {
      data: null,
      code: 200,
      msg: '删除成功'
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作'
    };
  }

  next();
};

exports.removeProject = removeProject;