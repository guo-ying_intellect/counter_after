"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.removeCost = exports.updateCost = exports.addCost = exports.getCostById = exports.getCostList = void 0;

var _auth = require("./auth");

var _Cost = _interopRequireDefault(require("../models/Cost"));

var _sequelize = require("sequelize");

var _utils = require("../lib/utils");

// 获取设备列表 手机号/用户名模糊查询
const getCostList = async (ctx, next) => {
  let {
    searchText
  } = ctx.request.body;
  let list;

  if (searchText) {
    list = await _Cost.default.findAll({
      where: {
        [_sequelize.Op.or]: [{
          cost_name: {
            [_sequelize.Op.like]: `%${searchText}%` //模糊搜索

          }
        }]
      }
    });
  } else {
    list = await _Cost.default.findAll();
  }

  ctx.body = {
    code: 200,
    data: list,
    msg: '请求成功'
  };
  next();
}; // 获取设备详情


exports.getCostList = getCostList;

const getCostById = async (ctx, next) => {
  let {
    cost_id
  } = ctx.request.body;
  let isAdmin = await (0, _utils.checkUserIsAdmin)(ctx);

  if (isAdmin && cost_id) {
    let cost = await _Cost.default.findOne({
      where: {
        cost_id
      }
    });
    ctx.body = {
      data: cost,
      code: 200,
      msg: '成功'
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作'
    };
  }

  next();
}; // 新增费用类型


exports.getCostById = getCostById;

const addCost = async (ctx, next) => {
  let {
    cost_name,
    cost_value,
    coef
  } = ctx.request.body;
  let isAdmin = await (0, _utils.checkUserIsAdmin)(ctx);

  if (isAdmin) {
    let cost = await _Cost.default.create({
      cost_name,
      cost_value,
      coef,
      create_time: +new Date(),
      update_time: +new Date()
    });
    ctx.body = {
      data: null,
      code: 200,
      msg: '费用添加成功'
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作'
    };
  }

  next();
}; // 更新设备信息


exports.addCost = addCost;

const updateCost = async (ctx, next) => {
  let {
    cost_id,
    cost_name,
    cost_value,
    coef
  } = ctx.request.body;
  let isAdmin = await (0, _utils.checkUserIsAdmin)(ctx);

  if (isAdmin && cost_id) {
    let device = await _Cost.default.update({
      cost_name,
      cost_value,
      coef,
      update_time: +new Date()
    }, {
      where: {
        cost_id
      }
    });
    ctx.body = {
      data: null,
      code: 200,
      msg: '费用更新成功'
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作'
    };
  }

  next();
}; // 删除设备


exports.updateCost = updateCost;

const removeCost = async (ctx, next) => {
  let {
    cost_id
  } = ctx.request.body;
  let isAdmin = await (0, _utils.checkUserIsAdmin)(ctx);

  if (isAdmin && cost_id) {
    let cost = await _Cost.default.destroy({
      where: {
        cost_id
      }
    });
    ctx.body = {
      data: null,
      code: 200,
      msg: '删除成功'
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作'
    };
  }

  next();
};

exports.removeCost = removeCost;