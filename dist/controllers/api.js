"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Delete = exports.Put = exports.Post = exports.Get = void 0;

const Get = (ctx, next) => {
  ctx.body = {
    result: 'get',
    name: ctx.params.name,
    para: ctx.query
  };
  next();
};

exports.Get = Get;

const Post = async (ctx, next) => {
  ctx.body = {
    result: 'post',
    name: ctx.params.name,
    para: ctx.request.body
  };
  next();
};

exports.Post = Post;

const Put = (ctx, next) => {
  ctx.body = {
    result: 'put',
    name: ctx.params.name,
    para: ctx.request.body
  };
  next();
};

exports.Put = Put;

const Delete = (ctx, next) => {
  ctx.body = {
    result: 'delete',
    name: ctx.params.name,
    para: ctx.request.body
  };
  next();
};

exports.Delete = Delete;