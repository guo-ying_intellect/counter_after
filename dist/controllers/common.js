"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getUserStatistics = void 0;

var _User = _interopRequireDefault(require("../models/User"));

var _Device = _interopRequireDefault(require("../models/Device"));

var _Cost = _interopRequireDefault(require("../models/Cost"));

var _Project = _interopRequireDefault(require("../models/Project"));

var _sequelize = require("sequelize");

var _utils = require("../lib/utils");

// 获取设备列表 手机号/用户名模糊查询
const getUserStatistics = async (ctx, next) => {
  let isAdmin = await (0, _utils.checkUserIsAdmin)(ctx);

  if (isAdmin) {
    let device = await _Device.default.findAll();
    let user = await _User.default.findAll();
    let cost = await _Cost.default.findAll();
    let project = await _Project.default.findAll();
    ctx.body = {
      code: 200,
      data: {
        deviceNumber: device.length,
        userNumber: user.length,
        costNumber: cost.length,
        projectNumber: project.length
      },
      msg: '请求成功'
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作'
    };
  }

  next();
};

exports.getUserStatistics = getUserStatistics;