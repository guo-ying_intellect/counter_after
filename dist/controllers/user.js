"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.removeUser = exports.updateUser = exports.addUser = exports.getUserById = exports.getUserList = exports.userWXLogin = exports.userLogin = void 0;

var _auth = require("./auth");

var _User = _interopRequireDefault(require("../models/User"));

var _axios = _interopRequireDefault(require("axios"));

var _sequelize = require("sequelize");

var _utils = require("../lib/utils");

const appId = 'wx762de5e5fd85fe95';
const appSecret = 'ea3e174ef2769fa3d57b895be9247cb8';

async function findUserById(ctx) {
  let {
    userId
  } = ctx.request.body;
  if (!userId) return null;
  let user = await _User.default.findOne({
    where: {
      user_id: userId
    }
  });
  return user;
} // 用户登录 仅限已被管理员添加了手机号的情况下


const userLogin = async (ctx, next) => {
  let {
    userPhone
  } = ctx.request.body;
  let user = await (0, _utils.findUserByPhone)(ctx);

  if (user) {
    let token = _auth.Auth.sign(ctx, {
      userPhone
    });

    console.log(token);
    ctx.body = {
      code: 200,
      data: {
        token
      },
      msg: '请求成功'
    };
  } else {
    ctx.body = {
      code: 400,
      data: null,
      msg: '手机号不存在, 请联系管理员添加'
    };
  }

  next();
}; // 微信登录 通过code open_id


exports.userLogin = userLogin;

const userWXLogin = async (ctx, next) => {
  let {
    code
  } = ctx.request.body;
  let res = await _axios.default.get(`https://api.weixin.qq.com/cgi-bin/token?appid=${appId}&secret=${appSecret}&js_code=${code}&grant_type=client_credential`);
  console.log(res.data);
  let access_token = res.data.access_token;
  let userRes = await _axios.default.post(`https://api.weixin.qq.com/wxa/business/getuserphonenumber?access_token=${access_token}`, {
    code
  });
  console.log(userRes.data);
  ctx.body = {
    data: res.data,
    code: 200,
    msg: '成功'
  };
  next();
}; // 获取用户列表 手机号/用户名模糊查询


exports.userWXLogin = userWXLogin;

const getUserList = async (ctx, next) => {
  let {
    searchText
  } = ctx.request.body;
  let list;

  if (searchText) {
    list = await _User.default.findAll({
      where: {
        [_sequelize.Op.or]: [{
          user_phone: {
            [_sequelize.Op.like]: `%${searchText}%` //模糊搜索

          }
        }, {
          user_name: {
            [_sequelize.Op.like]: `%${searchText}%`
          }
        }]
      }
    });
  } else {
    list = await _User.default.findAll();
  }

  ctx.body = {
    code: 200,
    data: list,
    msg: '请求成功'
  };
  next();
}; // 获取用户详情


exports.getUserList = getUserList;

const getUserById = async (ctx, next) => {
  let {
    user_id
  } = ctx.request.body;
  let user_phone;

  if (!user_id) {
    let {
      data
    } = (0, _auth.CheckAuth)(ctx);
    user_phone = data;
  }

  let isAdmin = await (0, _utils.checkUserIsAdmin)(ctx);

  if (isAdmin) {
    let user = await _User.default.findOne({
      where: {
        [_sequelize.Op.or]: [{
          user_id: user_id || ''
        }, {
          user_phone: user_phone || ''
        }]
      }
    });
    ctx.body = {
      data: user,
      code: 200,
      msg: '成功'
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作'
    };
  }

  next();
}; // 新增用户


exports.getUserById = getUserById;

const addUser = async (ctx, next) => {
  let {
    userName,
    userPhone,
    userRole
  } = ctx.request.body;
  let isAdmin = await (0, _utils.checkUserIsAdmin)(ctx);

  if (isAdmin) {
    let user = await _User.default.create({
      user_name: userName || userPhone,
      user_phone: userPhone,
      create_time: +new Date(),
      user_role: userRole || 2,
      // 默认创建2类用户
      nick_name: userName || userPhone
    });
    ctx.body = {
      data: null,
      code: 200,
      msg: '用户创建成功'
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作'
    };
  }

  next();
}; // 更新用户


exports.addUser = addUser;

const updateUser = async (ctx, next) => {
  let {
    user_name,
    user_phone,
    user_role,
    user_id,
    nick_name
  } = ctx.request.body;
  let isAdmin = await (0, _utils.checkUserIsAdmin)(ctx);

  if (isAdmin) {
    let user = await _User.default.update({
      user_name,
      user_phone,
      user_role,
      // 默认创建2类用户
      nick_name: nick_name || user_name
    }, {
      where: {
        user_id
      }
    });
    ctx.body = {
      data: null,
      code: 200,
      msg: '用户更新成功'
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作'
    };
  }

  next();
}; // 删除用户


exports.updateUser = updateUser;

const removeUser = async (ctx, next) => {
  let {
    user_id
  } = ctx.request.body;
  let isAdmin = await (0, _utils.checkUserIsAdmin)(ctx);
  console.log('isAdmin', isAdmin);

  if (isAdmin) {
    let user = await _User.default.destroy({
      where: {
        user_id: user_id
      }
    });
    ctx.body = {
      data: null,
      code: 200,
      msg: '删除成功'
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作'
    };
  }

  next();
};

exports.removeUser = removeUser;