"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _koaRouter = _interopRequireDefault(require("koa-router"));

var _controllers = _interopRequireDefault(require("../controllers"));

const router = new _koaRouter.default({
  prefix: '/device'
});

var _default = router.post('/list', _controllers.default.device.getDeviceList).post('/add', _controllers.default.device.addDevice).post('/update', _controllers.default.device.updateDevice).post('/detail', _controllers.default.device.getDeviceById).post('/remove', _controllers.default.device.removeDevice);

exports.default = _default;