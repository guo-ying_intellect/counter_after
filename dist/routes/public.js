"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _koaRouter = _interopRequireDefault(require("koa-router"));

var _file = require("../tool/file");

var _fs = _interopRequireDefault(require("fs"));

var _path = _interopRequireDefault(require("path"));

// import controllers from '../controllers'
const router = new _koaRouter.default();

var _default = router.get("/public", ctx => {
  ctx.body = "欢迎访问!!!!";
}); // 注意: 此路径放在最后
// 以/public开头则不经过权限认证
// .all('/upload', controllers.upload)
// .get('/public/api/:name', controllers.api.Get)
// .post('/api/:name', controllers.api.Post)
// .put('/api/:name', controllers.api.Put)
// .del('/api/:name', controllers.api.Delete)
// .post('/auth/:action', controllers.auth.Post)


exports.default = _default;