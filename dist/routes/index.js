"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initLoadRouters = initLoadRouters;

var _public = _interopRequireDefault(require("./public"));

var _user = _interopRequireDefault(require("./user"));

var _device = _interopRequireDefault(require("./device"));

var _cost = _interopRequireDefault(require("./cost"));

var _project = _interopRequireDefault(require("./project"));

var _common = _interopRequireDefault(require("./common"));

var _errorRoutes = _interopRequireDefault(require("./error-routes"));

// 加载全部路由
function initLoadRouters(app) {
  app.use(_public.default.routes());
  app.use(_user.default.routes());
  app.use(_device.default.routes());
  app.use(_cost.default.routes());
  app.use(_project.default.routes());
  app.use(_common.default.routes());
  app.use((0, _errorRoutes.default)());
}