"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _koaRouter = _interopRequireDefault(require("koa-router"));

var _controllers = _interopRequireDefault(require("../controllers"));

var _project = require("../controllers/project");

const router = new _koaRouter.default({
  prefix: '/project'
});

var _default = router.post('/list', _controllers.default.project.getProjectList).post('/add', _controllers.default.project.addProject).post('/update', _controllers.default.project.updateProject).post('/detail', _controllers.default.project.getProjectById).post('/remove', _controllers.default.project.removeProject);

exports.default = _default;