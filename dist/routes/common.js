"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _koaRouter = _interopRequireDefault(require("koa-router"));

var _controllers = _interopRequireDefault(require("../controllers"));

const router = new _koaRouter.default({
  prefix: '/common'
});

var _default = router.post('/statistics', _controllers.default.common.getUserStatistics);

exports.default = _default;