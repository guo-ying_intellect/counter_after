"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _koaRouter = _interopRequireDefault(require("koa-router"));

var _controllers = _interopRequireDefault(require("../controllers"));

const router = new _koaRouter.default({
  prefix: '/user'
});

var _default = router.post('/login', _controllers.default.user.userLogin).post('/list', _controllers.default.user.getUserList).post('/add', _controllers.default.user.addUser).post('/update', _controllers.default.user.updateUser).post('/detail', _controllers.default.user.getUserById).post('/remove', _controllers.default.user.removeUser); // .post('/wxlogin', controllers.user.userWXLogin)


exports.default = _default;