"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _koaRouter = _interopRequireDefault(require("koa-router"));

var _controllers = _interopRequireDefault(require("../controllers"));

const router = new _koaRouter.default({
  prefix: '/cost'
});

var _default = router.post('/list', _controllers.default.cost.getCostList).post('/add', _controllers.default.cost.addCost).post('/update', _controllers.default.cost.updateCost).post('/detail', _controllers.default.cost.getCostById).post('/remove', _controllers.default.cost.removeCost);

exports.default = _default;