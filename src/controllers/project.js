import { CheckAuth } from './auth';
import projectModal from "../models/Project"
import { Op, literal } from 'sequelize';
import { checkUserIsAdmin, checkUserIsActive, findUserByPhone } from "../lib/utils"
import project from "../models/Project"

// 获取工程列表 手机号/用户名模糊查询
export const getProjectList = async (ctx, next) => {
  let { searchText, pageSize } = ctx.request.body;
  let list
  let option = {
    order: [
      ['create_time', 'DESC']
    ]
  }
  if (searchText) {
    option.where =  {
      [Op.or]: [
        {
          name: {
            [Op.like]: `%${searchText}%`  //模糊搜索
          }
        }
      ]
    }
  }

  if (pageSize) {
    option.limit = pageSize
  }

  list = await projectModal.findAll(option);

  ctx.body = {
    code: 200,
    data: list,
    msg: '请求成功',
  };
  next();
};

// 获取设备详情
export const getProjectById = async (ctx, next) => {
  let { id } = ctx.request.body;
  let isAdmin = await checkUserIsActive(ctx)
  if (isAdmin && id) {
    let project = await projectModal.findOne({
      where: {
        id
      }
    });
    ctx.body = {
      data: project,
      code: 200,
      msg: '成功',
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作',
    };
  }


  next();
};
// 新增费用类型
export const addProject = async (ctx, next) => {
  let { name, devices, costs, amount, status, project_type, price_type, designCostJson, structure_area } = ctx.request.body;
  let isAdmin = await checkUserIsActive(ctx)
  if (isAdmin) {
    let { data } = CheckAuth(ctx);
    let user = await findUserByPhone(null, data);
    let cost = await projectModal.create({
      name, devices, costs, amount, project_type, price_type, designCostJson,structure_area,
      user_id: user.user_id,
      user_name: user.user_name,
      status: status || 1, // 状态暂时不需要
      create_time: +new Date(),
      update_time: +new Date()
    });
    ctx.body = {
      data: null,
      code: 200,
      msg: '项目添加成功',
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作',
    };
  }

  next();
};

// 更新项目信息
export const updateProject = async (ctx, next) => {
  let { id, name, devices, costs, amount, status, project_type, price_type, designCostJson, structure_area } = ctx.request.body;
  let isAdmin = await checkUserIsAdmin(ctx)
  if (isAdmin && id) {
    let { data } = CheckAuth(ctx);
    let user = await findUserByPhone(null, data);
    // TODO 增加非创建人能否修改
    let project = await projectModal.update({
      name, devices, costs, amount, project_type, price_type, designCostJson, structure_area,
      status: status || 1, // 状态暂时不需要
      update_time: +new Date()
    }, {
      where: {
        id
      }
    });
    ctx.body = {
      data: null,
      code: 200,
      msg: '费用更新成功',
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作',
    };
  }

  next();
};

// 删除设备
export const removeProject = async (ctx, next) => {
  let { id } = ctx.request.body;
  let isAdmin = await checkUserIsAdmin(ctx)
  if (isAdmin && id) {
    let project = await projectModal.destroy({
      where: {
        id
      }
    });
    ctx.body = {
      data: null,
      code: 200,
      msg: '删除成功',
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作',
    };
  }


  next();
};

