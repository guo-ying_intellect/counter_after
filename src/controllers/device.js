import { CheckAuth } from './auth';
import deviceModal from "../models/Device"
import { Op } from 'sequelize';
import { checkUserIsAdmin12, checkUserIsActive, findUserByPhone } from "../lib/utils"

// 获取设备列表 手机号/用户名模糊查询
export const getDeviceList = async (ctx, next) => {
  let { searchText, type1_id, type2_id, type3_id } = ctx.request.body;
  let list
  let option = {
    where: {}
  }

  if (searchText) {
    option.where = {
      [Op.or]: [
        {
          name: {
            [Op.like]: `%${searchText}%`  //模糊搜索
          }
        }
      ]
    }
  }

  if (type1_id) {
    option.where.type1_id = type1_id
  }

  if (type2_id) {
    option.where.type2_id = type2_id
  }

  if (type3_id) {
    option.where.type3_id = type3_id
  }

  list = await deviceModal.findAll(option);

  ctx.body = {
    code: 200,
    data: list,
    msg: '请求成功',
  };
  next();
};

// 获取设备详情
export const getDeviceById = async (ctx, next) => {
  let { id } = ctx.request.body;
  let isAdmin = await checkUserIsActive(ctx)
  if (isAdmin && id) {
    let device = await deviceModal.findOne({
      where: {
        id: id
      }
    });
    ctx.body = {
      data: device,
      code: 200,
      msg: '成功',
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作',
    };
  }


  next();
};
// 新增设备
export const addDevice = async (ctx, next) => {
  let { name, device_cost, device_cost_gong, desc, salary, install_cost, debug_cost, unit, type1_id, type2_id, type3_id} = ctx.request.body;
  let isAdmin = await checkUserIsAdmin12(ctx)
  if (isAdmin) {
    let { data } = CheckAuth(ctx);
    let user = await findUserByPhone(null, data);
    let device = await deviceModal.create({
      name,
      device_cost,
      debug_cost,
      device_cost_gong,
      desc,
      salary,
      install_cost,
      unit,
      create_time: +new Date(),
      update_time: +new Date(),
      user_id: user.user_id,
      user_name: user.user_name,
      type1_id, type2_id, type3_id
    });
    ctx.body = {
      data: null,
      code: 200,
      msg: '设备添加成功',
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作',
    };
  }

  next();
};

// 更新设备信息
export const updateDevice = async (ctx, next) => {
  let { id, name, device_cost, device_cost_gong, desc, salary, install_cost, debug_cost, unit, type1_id, type2_id, type3_id } = ctx.request.body;
  let isAdmin = await checkUserIsAdmin12(ctx)
  if (isAdmin && id) {
    let { data } = CheckAuth(ctx);
    let user = await findUserByPhone(null, data);
    let device = await deviceModal.update({
      name, device_cost, device_cost_gong, debug_cost, desc, salary, install_cost, unit,
      update_time: +new Date(),
      type1_id, type2_id, type3_id,
      user_id: user.user_id,
      user_name: user.user_name
    }, {
      where: {
        id
      }
    });
    ctx.body = {
      data: null,
      code: 200,
      msg: '设备更新成功',
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作',
    };
  }

  next();
};

// 删除设备
export const removeDevice = async (ctx, next) => {
  let { id } = ctx.request.body;
  let isAdmin = await checkUserIsAdmin12(ctx)
  if (isAdmin && id) {
    let device = await deviceModal.destroy({
      where: {
        id
      }
    });
    ctx.body = {
      data: null,
      code: 200,
      msg: '删除成功',
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作',
    };
  }


  next();
};

