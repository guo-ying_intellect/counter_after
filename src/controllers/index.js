import upload from './upload'
import * as api from './api'
import * as auth from './auth'
import * as user from './user'
import * as device from './device'
import * as cost from './cost'
import * as project from './project'
import * as Enum from './enum'
import * as common from './common'
export default {
  upload,
  api,
  auth,
  user,
  device,
  cost,
  project,
  Enum,
  common
}
