import { CheckAuth } from './auth';
import enumModal from "../models/Enum"
import { Op } from 'sequelize';
import { checkUserIsActive, checkUserIsAdmin12, findUserByPhone } from "../lib/utils"

export const getEnumList = async (ctx, next) => {
  let { searchText } = ctx.request.body;
  let level = 0;
  let list
  let option = {}
  if (searchText) {
    option.where = {
      [Op.or]: [
        {
          cost_name: {
            [Op.like]: `%${searchText}%`  //模糊搜索
          }
        }
      ]
    }
  }
  // 获取level1
  if (level) {
    if (!option.where) {
      option.where = {}
    }
    option.where.level = level
  }
  list = await enumModal.findAll(option);

  ctx.body = {
    code: 200,
    data: list,
    msg: '请求成功',
  };
  next();
};

// 获取所有第一级枚举值  支持名称搜索
export const getEnumLevel1List = async (ctx, next) => {
  let { searchText } = ctx.request.body;
  let level = 1;
  let list
  let option = {}
  if (searchText) {
    option.where = {
      [Op.or]: [
        {
          cost_name: {
            [Op.like]: `%${searchText}%`  //模糊搜索
          }
        }
      ]
    }
  }
  // 获取level1
  if (level) {
    if (!option.where) {
      option.where = {}
    }
    option.where.level = level
  }
  list = await enumModal.findAll(option);

  ctx.body = {
    code: 200,
    data: list,
    msg: '请求成功',
  };
  next();
};

// 获取所有第23级枚举值 根据一级type
export const getEnumLevel23List = async (ctx, next) => {
  let { enum_type, value } = ctx.request.body;
  let list
  let option = {}
  if (enum_type) {
    if (!option.where) {
      option.where = {}
    }
    option.where.enum_type = enum_type
    list = await enumModal.findAll(option);
  } else if (value) {
    let enumDetail = await enumModal.findOne({
      where: {
        value
      }
    });
    list = await enumModal.findAll({
      where: {
        enum_type: enumDetail.enum_type
      }
    })
  } else {
    list = []
  }

  let list2 = list.filter( item => item.level == 2);
  let list3 = list.filter( item => item.level == 3)
  ctx.body = {
    code: 200,
    data: {
      list,
      list2,
      list3
    },
    msg: '请求成功',
  };
  next();
};

// 获取枚举值详情
export const getEnumById = async (ctx, next) => {
  let { id } = ctx.request.body;
  let isAdmin = await checkUserIsActive(ctx)
  if (isAdmin && id) {
    let enumDetail = await enumModal.findOne({
      where: {
        id
      }
    });
    ctx.body = {
      data: enumDetail,
      code: 200,
      msg: '成功',
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作',
    };
  }


  next();
};
// 新增费用类型
export const addEnum = async (ctx, next) => {
  let { enum_type, label, value, level } = ctx.request.body;
  let isAdmin = await checkUserIsAdmin12(ctx)
  if (isAdmin) {
    let { data } = CheckAuth(ctx);
    let user = await findUserByPhone(null, data);
    let cost = await enumModal.create({
      enum_type, label, value, level,
      create_time: +new Date(),
      update_time: +new Date(),
      user_id: user.user_id,
      user_name: user.user_name
    });
    ctx.body = {
      data: null,
      code: 200,
      msg: '枚举添加成功',
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作',
    };
  }

  next();
};

// 更新枚举值信息
export const updateEnum = async (ctx, next) => {
  let { enum_type, label, value, id } = ctx.request.body;
  let isAdmin = await checkUserIsAdmin12(ctx)
  if (isAdmin && id) {
    let { data } = CheckAuth(ctx);
    let user = await findUserByPhone(null, data);
    let _enum = await enumModal.update({
      enum_type, label, value,
      update_time: +new Date(),
      user_id: user.user_id,
      user_name: user.user_name
    }, {
      where: {
        id
      }
    });
    ctx.body = {
      data: null,
      code: 200,
      msg: '费用更新成功',
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作',
    };
  }

  next();
};

// 删除枚举
export const removeEnum = async (ctx, next) => {
  let { id } = ctx.request.body;
  let isAdmin = await checkUserIsAdmin12(ctx)
  if (isAdmin && id) {
    let cost = await enumModal.destroy({
      where: {
        id
      }
    });
    ctx.body = {
      data: null,
      code: 200,
      msg: '删除成功',
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作',
    };
  }


  next();
};

