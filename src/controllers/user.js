import { Auth, CheckAuth } from './auth';
import userModal from "../models/User"
import axios from 'axios'
import { Op } from 'sequelize'
const appId = 'wx762de5e5fd85fe95'
const appSecret = 'ea3e174ef2769fa3d57b895be9247cb8'
import { checkUserIsAdmin, findUserByPhone} from "../lib/utils"

async function findUserById(ctx) {
  let { userId } = ctx.request.body;
  if (!userId) return null;
  let user =  await userModal.findOne({
    where: {
      user_id: userId
    }
  })
  return user
}
// 用户登录 仅限已被管理员添加了手机号的情况下
export const userLogin = async (ctx, next) => {
  let { userPhone } = ctx.request.body;
  let user = await findUserByPhone(ctx);
  if (user) {
    let token = Auth.sign(ctx, { userPhone });
    console.log(token)
    ctx.body = {
      code: 200,
      data: {
        token,
      },
      msg: '请求成功',
    };
  } else {
    ctx.body = {
      code: 400,
      data: null,
      msg: '手机号不存在, 请联系管理员添加',
    };
  }


  next();
};
// 微信登录 通过code open_id
export const userWXLogin = async (ctx, next) => {
  let { code } = ctx.request.body;
  let res = await axios.get(`https://api.weixin.qq.com/cgi-bin/token?appid=${appId}&secret=${appSecret}&js_code=${code}&grant_type=client_credential`)
  console.log(res.data)
  let access_token = res.data.access_token;
    let userRes = await axios.post(`https://api.weixin.qq.com/wxa/business/getuserphonenumber?access_token=${access_token}`, {
      code
    })
  console.log(userRes.data)
  ctx.body = {
    data: res.data,
    code: 200,
    msg: '成功',
  };
  next();
};
// 获取用户列表 手机号/用户名模糊查询
export const getUserList = async (ctx, next) => {
  let { searchText } = ctx.request.body;
  let list
  if (searchText) {
    list = await userModal.findAll({
      where: {
        [Op.or]: [
          {
            user_phone: {
              [Op.like]: `%${searchText}%`  //模糊搜索
            }
            },
          {
            user_name: {
              [Op.like]: `%${searchText}%`
            }
          }
        ]
      }
    });
  } else {
    list = await userModal.findAll();
  }

  ctx.body = {
    code: 200,
    data: list,
    msg: '请求成功',
  };
  next();
};

// 获取用户详情
export const getUserById = async (ctx, next) => {
  let { user_id } = ctx.request.body;
  let user_phone;
  if (!user_id) {
    let { data } = CheckAuth(ctx);
    user_phone = data;
  }
  let user = await userModal.findOne({
    where: {
      [Op.or]: [
        { user_id: user_id || '' },
        { user_phone: user_phone || '' }
      ]
    }
  });
  ctx.body = {
    data: user,
    code: 200,
    msg: '成功',
  };


  next();
};
// 新增用户
export const addUser = async (ctx, next) => {
  let { userName, userPhone, userRole } = ctx.request.body;
  let isAdmin = await checkUserIsAdmin(ctx)
  if (isAdmin) {
    let user = await userModal.create({
      user_name: userName || userPhone,
      user_phone: userPhone,
      create_time: +new Date(),
      user_role: userRole || 2, // 默认创建2类用户
      nick_name: userName || userPhone
    });
    ctx.body = {
      data: null,
      code: 200,
      msg: '用户创建成功',
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作',
    };
  }

  next();
};

// 更新用户
export const updateUser = async (ctx, next) => {
  let { user_name, user_phone, user_role, user_id, nick_name } = ctx.request.body;
  let isAdmin = await checkUserIsAdmin(ctx)
  if (isAdmin) {
    let user = await userModal.update({
      user_name,
      user_phone,
      user_role, // 默认创建2类用户
      nick_name: nick_name || user_name
    }, {
      where: {
        user_id
      }
    });
    ctx.body = {
      data: null,
      code: 200,
      msg: '用户更新成功',
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作',
    };
  }

  next();
};

// 删除用户
export const removeUser = async (ctx, next) => {
  let { user_id } = ctx.request.body;
  let isAdmin = await checkUserIsAdmin(ctx)
  console.log('isAdmin', isAdmin)
  if (isAdmin) {
    let user = await userModal.destroy({
      where: {
        user_id: user_id
      }
    });
    ctx.body = {
      data: null,
      code: 200,
      msg: '删除成功',
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作',
    };
  }


  next();
};

