import { CheckAuth } from './auth';
import costModal from "../models/Cost"
import { Op } from 'sequelize';
import { checkUserIsAdmin12, findUserByPhone } from "../lib/utils"

// 获取设备列表 手机号/用户名模糊查询
export const getCostList = async (ctx, next) => {
  let { searchText } = ctx.request.body;
  let list
  if (searchText) {
    list = await costModal.findAll({
      where: {
        [Op.or]: [
          {
            cost_name: {
              [Op.like]: `%${searchText}%`  //模糊搜索
            }
          }
        ]
      }
    });
  } else {
    list = await costModal.findAll();
  }

  ctx.body = {
    code: 200,
    data: list,
    msg: '请求成功',
  };
  next();
};

// 获取设备详情
export const getCostById = async (ctx, next) => {
  let { cost_id } = ctx.request.body;
  let isAdmin = await checkUserIsAdmin12(ctx)
  if (isAdmin && cost_id) {
    let cost = await costModal.findOne({
      where: {
        cost_id
      }
    });
    ctx.body = {
      data: cost,
      code: 200,
      msg: '成功',
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作',
    };
  }


  next();
};
// 新增费用类型
export const addCost = async (ctx, next) => {
  let { cost_name, cost_value, coef, type_id, type_name, } = ctx.request.body;
  let isAdmin = await checkUserIsAdmin12(ctx)
  if (isAdmin) {
    let { data } = CheckAuth(ctx);
    let user = await findUserByPhone(null, data);
    let cost = await costModal.create({
      cost_name, cost_value, coef,
      create_time: +new Date(),
      update_time: +new Date(),
      user_id: user.user_id,
      user_name: user.user_name,
      type_id, type_name,
    });
    ctx.body = {
      data: null,
      code: 200,
      msg: '费用添加成功',
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作',
    };
  }

  next();
};

// 更新设备信息
export const updateCost = async (ctx, next) => {
  let { cost_id, cost_name, cost_value, coef, type_id, type_name, } = ctx.request.body;
  let isAdmin = await checkUserIsAdmin12(ctx)
  if (isAdmin && cost_id) {
    let { data } = CheckAuth(ctx);
    let user = await findUserByPhone(null, data);
    let device = await costModal.update({
      cost_name, cost_value, coef,
      update_time: +new Date(),
      user_id: user.user_id,
      user_name: user.user_name,
      type_id, type_name,
    }, {
      where: {
        cost_id
      }
    });
    ctx.body = {
      data: null,
      code: 200,
      msg: '费用更新成功',
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作',
    };
  }

  next();
};

// 删除设备
export const removeCost = async (ctx, next) => {
  let { cost_id } = ctx.request.body;
  let isAdmin = await checkUserIsAdmin12(ctx)
  if (isAdmin && cost_id) {
    let cost = await costModal.destroy({
      where: {
        cost_id
      }
    });
    ctx.body = {
      data: null,
      code: 200,
      msg: '删除成功',
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作',
    };
  }


  next();
};

