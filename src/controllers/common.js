import userModal from "../models/User"
import deviceModal from "../models/Device"
import CostModal from '../models/Cost'
import ProjectModal from '../models/Project'
import EnumModal from '../models/Enum'
import { Op } from 'sequelize'
import { checkUserIsActive, findUserByPhone } from "../lib/utils"


// 获取设备列表 手机号/用户名模糊查询
export const getUserStatistics = async (ctx, next) => {
  let user = await checkUserIsActive(ctx)
  if (!!user) {
    let device = await deviceModal.findAll();
    let user = await userModal.findAll();
    let cost = await CostModal.findAll();
    let project = await ProjectModal.findAll()
    let enums = await EnumModal.findAll({
      where: {
        enum_type: 1
      }
    })
    ctx.body = {
      code: 200,
      data: {
        deviceNumber: device.length,
        userNumber: user.length,
        costNumber: cost.length,
        projectNumber: project.length,
        enumNumber: enums.length
      },
      msg: '请求成功',
    };
  } else {
    ctx.body = {
      data: null,
      code: 400,
      msg: '您没有此权限, 请联系管理员操作',
    };
  }
  next();
};
