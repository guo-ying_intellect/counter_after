import KoaRouter from "koa-router";
// import controllers from '../controllers'
import { getCityUnionJsonFile, getCityUnionFilePath } from "../tool/file";
import fs from "fs";
import path from "path";

const router = new KoaRouter();

export default router
  .get("/public", (ctx) => {
    ctx.body = "欢迎访问!!!!";
  })
  // 注意: 此路径放在最后

// 以/public开头则不经过权限认证
// .all('/upload', controllers.upload)
// .get('/public/api/:name', controllers.api.Get)
// .post('/api/:name', controllers.api.Post)
// .put('/api/:name', controllers.api.Put)
// .del('/api/:name', controllers.api.Delete)
// .post('/auth/:action', controllers.auth.Post)
