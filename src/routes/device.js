import KoaRouter from 'koa-router'
import controllers from '../controllers'

const router = new KoaRouter({
    prefix: '/device'
})

export default router
    .post('/list', controllers.device.getDeviceList)
    .post('/add', controllers.device.addDevice)
    .post('/update', controllers.device.updateDevice)
    .post('/detail', controllers.device.getDeviceById)
    .post('/remove', controllers.device.removeDevice)
