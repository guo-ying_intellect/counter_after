import KoaRouter from 'koa-router'
import controllers from '../controllers'
import {getEnumLevel23List} from "../controllers/enum"

const router = new KoaRouter({
  prefix: '/common'
})

export default router
  .post('/statistics', controllers.common.getUserStatistics)
  .post('/allTypes', controllers.Enum.getEnumList)
  .post('/level1Types', controllers.Enum.getEnumLevel1List)
  .post('/level23Types', controllers.Enum.getEnumLevel23List)
  .post('/typeDetail', controllers.Enum.getEnumById)
  .post('/type/add', controllers.Enum.addEnum)
  .post('/type/update', controllers.Enum.updateEnum)
  .post('/type/remove', controllers.Enum.removeEnum)
