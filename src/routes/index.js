import PublicRoutes from "./public";
import UserRouters from './user'
import DeviceRouters from './device'
import CostRouters from './cost'
import ProjectRouters from './project'
import CommonRouters from './common'
import ErrorRoutes from "./error-routes";

// 加载全部路由
export function initLoadRouters(app) {
  app.use(PublicRoutes.routes());
  app.use(UserRouters.routes())
  app.use(DeviceRouters.routes())
  app.use(CostRouters.routes())
  app.use(ProjectRouters.routes())
  app.use(CommonRouters.routes())
  app.use(ErrorRoutes());
}
