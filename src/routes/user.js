import KoaRouter from 'koa-router'
import controllers from '../controllers'

const router = new KoaRouter({
    prefix: '/user'
})

export default router
    .post('/login', controllers.user.userLogin)
    .post('/list', controllers.user.getUserList)
    .post('/add', controllers.user.addUser)
    .post('/update', controllers.user.updateUser)
    .post('/detail', controllers.user.getUserById)
    .post('/remove', controllers.user.removeUser)

    // .post('/wxlogin', controllers.user.userWXLogin)
