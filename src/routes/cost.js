import KoaRouter from 'koa-router'
import controllers from '../controllers'

const router = new KoaRouter({
    prefix: '/cost'
})

export default router
    .post('/list', controllers.cost.getCostList)
    .post('/add', controllers.cost.addCost)
    .post('/update', controllers.cost.updateCost)
    .post('/detail', controllers.cost.getCostById)
    .post('/remove', controllers.cost.removeCost)
