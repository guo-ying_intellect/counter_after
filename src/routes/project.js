import KoaRouter from 'koa-router'
import controllers from '../controllers'
import {getProjectList} from "../controllers/project"

const router = new KoaRouter({
    prefix: '/project'
})

export default router
    .post('/list', controllers.project.getProjectList)
    .post('/add', controllers.project.addProject)
    .post('/update', controllers.project.updateProject)
    .post('/detail', controllers.project.getProjectById)
    .post('/remove', controllers.project.removeProject)
