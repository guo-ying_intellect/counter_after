import Koa2 from "koa";
import KoaBody from "koa-body";
import cors from "@koa/cors";
import KoaStatic from "koa-static2";
import { System as SystemConfig } from "./config";
import path from "path";
import { initLoadRouters } from "./routes";
import ErrorRoutesCatch from "./middleware/ErrorRoutesCatch";
import jwt from "koa-jwt";
import fs from "fs";
// import PluginLoader from './lib/PluginLoader'

const app = new Koa2();
const env = process.env.NODE_ENV || "development"; // Current mode

const publicKey = fs.readFileSync(path.join(__dirname, "../publicKey.pub"));

app
  .use(cors())
  .use(ErrorRoutesCatch())
  .use(KoaStatic("assets", path.resolve(__dirname, "../assets"))) // Static resource
  // 以一个动态的secret去加密
  .use(
    jwt({ secret: publicKey }).unless({
      path: [/^\/public|\/user\/login|\/assets/],
    })
  )

  .use(
    KoaBody({
      multipart: true,
      parsedMethods: ["POST", "PUT", "PATCH", "GET", "HEAD", "DELETE"], // parse GET, HEAD, DELETE requests
      formidable: {
        uploadDir: path.join(__dirname, "../assets/uploads/tmp"),
      },
      jsonLimit: "10mb",
      formLimit: "10mb",
      textLimit: "10mb",
    })
  ); // Processing request

// 加载路由111
initLoadRouters(app);

if (env === "development") {
  // logger
  app.use((ctx, next) => {
    const start = new Date();
    return next().then(() => {
      const ms = new Date() - start;
      console.log(`${ctx.method} ${ctx.url} - ${ms}ms`);
    });
  });
}

app.listen(SystemConfig.API_server_port);

console.log(
  "Now start API server on port " + SystemConfig.API_server_port + "..."
);

export default app;
