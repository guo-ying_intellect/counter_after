import fs from "fs";
import path from "path";

export const getCityUnionFilePath = (fileName) => {
  return path.resolve(__dirname, `../../city-union-json/${fileName}`);
};
export const getCityUnionJsonFile = async (filePath) => {
  return new Promise((resolve, reject) => {
    fs.readFile(filePath, { encoding: "utf-8" }, (err, fileStr) => {
      if (err) {
        resolve("{}");
      } else {
        resolve(fileStr);
      }
    });
  });
};
