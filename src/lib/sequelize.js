import {Sequelize} from 'sequelize'
import { DB as DBConfig, System as SystemConfig } from '../config'

const sequelize = new Sequelize(DBConfig.database, DBConfig.username, DBConfig.password, {
  host: DBConfig.host,
  port: DBConfig.port,
  dialect: SystemConfig.db_type,
  dialectOptions: { // MySQL > 5.5，其它数据库删除此项
    charset: 'utf8mb4',
    supportBigNumbers: true,
    bigNumberStrings: true
  },
  pool: {
    max: 50,
    min: 0,
    idle: 10000
  }
})

// 测试是否能连通
sequelize.authenticate().then(() => {
  console.log("连接数据库成功");
}).catch(err => {
  console.log("连接数据库失败", err);
});

export default sequelize
