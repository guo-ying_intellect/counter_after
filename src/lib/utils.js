import {CheckAuth} from "../controllers/auth"
import userModal from "../models/User"

export function formatDatetime(date) {
  if (!date) {
    return "";
  }

  if (typeof date == "number" || typeof date == 'string') {
    date = new Date(date);
  }

  function fullNumToStr(num) {
    return num < 10 ? `0${num}` : `${num}`
  }

  if (!date) return '-'
  let year = date.getFullYear()
  let month = fullNumToStr(date.getMonth() + 1)
  let day = fullNumToStr(date.getDate())
  let hour = fullNumToStr(date.getHours())
  let minute = fullNumToStr(date.getMinutes())
  let second = fullNumToStr(date.getSeconds())

  return `${year}-${month}-${day} ${hour}:${minute}:${second}`
}

// 检测用户是不是管理员  通过传入的id
export async function checkUserIsAdmin(ctx) {
  let { data } = CheckAuth(ctx);
  if (data) {
    let user = await findUserByPhone(null, data);
    return user && user.user_role === 1
  } else {
    return false;
  }
}

export async function checkUserIsAdmin12(ctx) {
  let { data } = CheckAuth(ctx);
  if (data) {
    let user = await findUserByPhone(null, data);
    return user && (user.user_role === 1 || user.user_role === 2)
  } else {
    return false;
  }
}
export async function checkUserIsActive(ctx) {
  let { data } = CheckAuth(ctx);
  if (data) {
    let user = await findUserByPhone(null, data);
    return !!user
  } else {
    return false;
  }
}
export async function findUserByPhone(ctx, phone) {
  let userPhone = phone || ctx.request.body.userPhone
  if (!userPhone) return null
  let user =  await userModal.findOne({
    where: {
      user_phone: userPhone
    }
  })
  return user
}
