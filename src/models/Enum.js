import sequelize from "../lib/sequelize"
import { DataTypes } from "sequelize"

const enumModal = sequelize.define('enum', {
  enum_type: DataTypes.INTEGER,
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  value: DataTypes.INTEGER,
  label: DataTypes.STRING,
  level: DataTypes.NUMBER,
  create_time: {
    type: DataTypes.BIGINT
  },
  update_time: {
    type: DataTypes.BIGINT
  },
  user_id: DataTypes.INTEGER
}, {
  freezeTableName: true,
  timestamps: false
})


export default enumModal;
