import sequelize from "../lib/sequelize"
import { DataTypes } from "sequelize"

const costModal = sequelize.define('cost', {
  cost_name: {
    type: DataTypes.STRING
  },
  cost_id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  cost_value: {
    type: DataTypes.DOUBLE
  },
  create_time: {
    type: DataTypes.BIGINT
  },
  update_time: {
    type: DataTypes.BIGINT
  },
  coef: {
    type: DataTypes.DOUBLE
  },
  user_id: DataTypes.INTEGER,
  user_name: DataTypes.STRING,
  type_id: DataTypes.INTEGER,
  type_name: DataTypes.STRING
}, {
  freezeTableName: true,
  timestamps: false
})


export default costModal;
