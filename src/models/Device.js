import sequelize from "../lib/sequelize"
import { DataTypes } from "sequelize"

const deviceModal = sequelize.define('device', {
  name: {
    type: DataTypes.STRING
  },
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  device_cost: {
    type: DataTypes.DOUBLE
  },
  device_cost_gong: DataTypes.DOUBLE,
  create_time: {
    type: DataTypes.BIGINT
  },
  update_time: {
    type: DataTypes.BIGINT
  },
  desc: {
    type: DataTypes.STRING
  },
  salary: {
    type: DataTypes.DOUBLE
  },
  install_cost: {
    type: DataTypes.DOUBLE
  },
  debug_cost: {
    type: DataTypes.DOUBLE
  },
  unit: {
    type: DataTypes.STRING
  },
  user_id: DataTypes.INTEGER,
  user_name: DataTypes.STRING,
  type_id: DataTypes.INTEGER,
  type1_id: DataTypes.INTEGER,
  type2_id: DataTypes.INTEGER,
  type3_id: DataTypes.INTEGER,
  type_name: DataTypes.STRING
}, {
  freezeTableName: true,
  timestamps: false
})


export default deviceModal;
