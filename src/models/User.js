import sequelize from "../lib/sequelize"
import { DataTypes } from "sequelize"

const userModal = sequelize.define('user', {
  user_name: {
    type: DataTypes.STRING
  },
  user_id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  user_phone: {
    type: DataTypes.INTEGER
  },
  create_time: {
    type: DataTypes.BIGINT
  },
  user_role: {
    type: DataTypes.INTEGER
  },
  nick_name: {
    type: DataTypes.STRING
  }
}, {
  freezeTableName: true,
  timestamps: false
})


export default userModal;
