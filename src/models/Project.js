import sequelize from "../lib/sequelize"
import { DataTypes } from "sequelize"

const projectModal = sequelize.define('project', {
  name: {
    type: DataTypes.STRING
  },
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  devices: {
    type: DataTypes.JSON
  },
  designCostJson: DataTypes.JSON,
  create_time: {
    type: DataTypes.BIGINT,
    field: 'create_time'
  },
  update_time: {
    type: DataTypes.BIGINT
  },
  costs: {
    type: DataTypes.JSON
  },
  amount: {
    type: DataTypes.DOUBLE
  },
  structure_area: DataTypes.DOUBLE,
  status: {
    type: DataTypes.INTEGER
  },
  user_id: {
    type: DataTypes.INTEGER
  },
  project_type: DataTypes.INTEGER,
  price_type: DataTypes.INTEGER,
  user_name: DataTypes.STRING
}, {
  freezeTableName: true,
  timestamps: false
})


export default projectModal;
